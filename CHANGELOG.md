## [1.3.0] - 2022-07-07
### Added
- Rates cache

## [1.2.0] - 2022-04-11
### Removed
- Shipping Zones support

## [1.1.6] - 2022-04-08
### Fixed 
- fallback cost field

## [1.1.5] - 2022-04-08
### Added
- Shipping Zones support - texts

## [1.1.4] - 2022-04-07
### Added
- Shipping Zones support

## [1.0.1] - 2021-07-28
### Fixed
- removed UPS libraries

## [1.0.0] - 2021-07-23
### Added
- initial version
