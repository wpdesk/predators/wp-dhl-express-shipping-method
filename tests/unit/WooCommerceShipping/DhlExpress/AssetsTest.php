<?php

class AssetsTest extends \WP_Mock\Tools\TestCase {
	/**
	 * @var \WPDesk\WooCommerceShipping\DhlExpress\Assets
	 */
	private $assets_under_tests;

	/**
	 * Set up.
	 */
	public function setUp() {
		\WP_Mock::setUp();

		$this->assets_under_tests = new \WPDesk\WooCommerceShipping\DhlExpress\Assets( 'url/', '11' );
	}

	/**
	 * Tear down.
	 */
	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function testShouldAddAction() {
		// Expected
		WP_Mock::expectActionAdded( 'admin_enqueue_scripts', [ $this->assets_under_tests, 'admin_enqueue_scripts' ] );

		// When
		$this->assets_under_tests->hooks();

		// Then
		$this->assertTrue( true );
	}

	public function testShouldEnqueueScriptOnSettings() {
		// Expected
		$handle = 'wpdesk_wc_shipping_dhl_express';
		WP_Mock::userFunction( 'wp_enqueue_script' )->once()->withArgs( array( $handle ) );
		WP_Mock::userFunction( 'wp_register_script' )->once()->withArgs( array( $handle, 'url/js/settings.js', [ 'jquery' ], '11' ) );
		WP_Mock::userFunction( 'trailingslashit' )->once()->andReturnArg( 0 );

		// Given
		global  $current_screen;
		$current_screen = new stdClass();
		$current_screen->id = 'woocommerce_page_wc-settings';

		// When
		$this->assets_under_tests->admin_enqueue_scripts();

		// Then
		$this->assertTrue( true );
	}

	public function testShouldNotEnqueueScriptOnOtherPages() {
		// Expected
		WP_Mock::userFunction( 'wp_enqueue_script' )->never();
		WP_Mock::userFunction( 'wp_register_script' )->never();
		WP_Mock::userFunction( 'trailingslashit' )->never();

		// Given
		global  $current_screen;
		$current_screen = new stdClass();
		$current_screen->id = 'other-page';

		// When
		$this->assets_under_tests->admin_enqueue_scripts();

		// Then
		$this->assertTrue( true );
	}

}
